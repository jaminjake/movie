package com.jaminjake.movie;

import  com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSON {

    public static String movieToJSON(Movie movieJSON) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(movieJSON);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }
    public static Movie JSONtoMovie(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Movie movieJSON = null;

        try {
            movieJSON = mapper.readValue(s, Movie.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return  movieJSON;
    }
}
