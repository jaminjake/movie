package com.jaminjake.movie;

/**
 * TestDAO.java
 * Course: CIT360
 * Name: Jake Evans
 * Week: 12 Project
 */

import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO() {
        factory = SessionFactoryUtils.getSessionFactory();
    }

    /**
     * Single Instance
     */
    public static TestDAO getInstance() {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }

    /**
     * Used to get more than one movie from the database session.
     */
    public List<Movie> getMovies() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.jaminjake.movie.Movie";
            List<Movie> movieList = (List<Movie>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return movieList;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /**
     * Used to get a single movie from the database
     */
    public Movie getMovie(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.jaminjake.movie.Movie where id=" + Integer.toString(id);
            Movie movie = (Movie) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return movie;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Movie addMovie(String movieName, String movieGenre, String movieRating, int year) {

        try {
            System.out.println("\nAdding movie to Database: " + movieName + " " + movieGenre + " " + movieRating);
            //movieName = addMovie(setMovieName);
            //List<Movie> addList = (List<Movie>);
            session = factory.openSession();
            session.beginTransaction().begin();
            String sql = "com.jaminjake.movie.Movie";
            //addMovie(movieName,movieGenre,movieRating);
            //Movie movie = (Movie) session.createStoredProcedureCall(sql).   createQuery(sql).getSingleResult();
            Movie movie = new Movie();
            movie.setMovieName(movieName);
            movie.setMovieGenre(movieGenre);
            movie.setMovieRating(movieRating);

            //Movie movie = (Movie) session.save(mv);
            //Movie movie = new Movie();

            // Save the Movie in the database
            session.save(movie);
            //Commit the transaction
            session.getTransaction().commit();
            //SessionFactoryUtils.shutdown();
            return null;
            //return addMovie(String movieName, String movieGenre, String movieRating);
        } catch (HibernateException e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}


