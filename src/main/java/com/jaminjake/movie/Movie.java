package com.jaminjake.movie;

/**
 * Movie.java
 * Course: CIT360
 * Name: Jake Evans
 * Week: 12 Project
 */


import javax.persistence.*;

/**
 * This file interacts with the movie database and the table movie
 */
@Entity
@Table(name = "movie")
public class Movie {
    private int movieId;
    private String movieName;
    private String movieGenre;
    private String movieRating;
    private int movieYear;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "movie_id", unique = true, nullable = false)
    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    @Basic
    @Column(name = "movieName", nullable = false, length = 100)
    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    @Basic
    @Column(name = "movieGenre", nullable = false, length = 45)
    public String getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    @Basic
    @Column(name = "movieRating", nullable = false, length = 45)
    public String getMovieRating() {
        return movieRating;
    }

    public void setMovieRating(String movieRating) {
        this.movieRating = movieRating;
    }

    @Basic
    @Column(name = "movieYear", nullable = false)
    public int getMovieYear() {
        return movieYear;
    }

    public void setMovieYear(int movieYear) {
        this.movieYear = movieYear;
    }

    /**
     * @Override public boolean equals(Object o) {
     * if (this == o) return true;
     * if (o == null || getClass() != o.getClass()) return false;
     * <p>
     * Movie that = (Movie) o;
     * <p>
     * if (movieId != that.movieId) return false;
     * if (movieName != null ? !movieName.equals(that.movieName) : that.movieName != null) return false;
     * if (movieGenre != null ? !movieGenre.equals(that.movieGenre) : that.movieGenre != null) return false;
     * if (movieRating != null ? !movieRating.equals(that.movieRating) : that.movieRating != null) return false;
     * <p>
     * return true;
     * }
     * @Override public int hashCode() {
     * int result = movieId;
     * result = 31 * result + (movieName != null ? movieName.hashCode() : 0);
     * result = 31 * result + (movieGenre != null ? movieGenre.hashCode() : 0);
     * result = 31 * result + (movieRating != null ? movieRating.hashCode() : 0);
     * return result;
     * }
     */
    public String toString() {
        return Integer.toString(movieId) + " " + movieName + " " + movieGenre + " " + movieRating + Integer.toString(movieYear);
    }

}
