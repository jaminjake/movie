package com.jaminjake.movie;

public class MainJSON {

    public static void main(String[] args) {

        Movie movie1 = new Movie();
        movie1.setMovieName("Bourne Identity");
        movie1.setMovieGenre("Action");
        movie1.setMovieRating("PG-13");
        movie1.setMovieYear(2002);

        String json1 = JSON.movieToJSON(movie1);
        System.out.println(json1);

        Movie movieAdd1 = JSON.JSONtoMovie(json1);
        System.out.println(movieAdd1);

        Movie movie2 = new Movie();
        movie2.setMovieName("The Sum of all Fears");
        movie2.setMovieGenre("Action");
        movie2.setMovieRating("PG-13");
        movie2.setMovieYear(2002);

        String json2 = JSON.movieToJSON(movie2);
        System.out.println(json2);

        Movie movieAdd2 = JSON.JSONtoMovie(json2);
        System.out.println(movieAdd2);

        Movie movie3 = new Movie();
        movie3.setMovieName("Anastasia");
        movie3.setMovieGenre("Family");
        movie3.setMovieRating("G");
        movie3.setMovieYear(1997);

        String json3 = JSON.movieToJSON(movie3);
        System.out.println(json3);

        Movie movieAdd3 = JSON.JSONtoMovie(json3);
        System.out.println(movieAdd3);


        Movie movie4 = new Movie();
        movie4.setMovieName("Armageddon");
        movie4.setMovieGenre("Action");
        movie4.setMovieRating("PG-13");
        movie4.setMovieYear(1998);

        String json4 = JSON.movieToJSON(movie4);
        System.out.println(json4);

        Movie movieAdd4 = JSON.JSONtoMovie(json4);
        System.out.println(movieAdd4);


        ;


    }
}
