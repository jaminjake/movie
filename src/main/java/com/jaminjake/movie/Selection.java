package com.jaminjake.movie;

/**
 * Selection.java
 * Course: CIT360
 * Name: Jake Evans
 * Week: 12 Project
 */

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet(name = "Selection", urlPatterns = "/Selection")
public class Selection extends HttpServlet {

    @Override

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        //Declare Variables
        boolean validAdd = false;
        String titleSearch = "";
        String titleAdd = "";
        int genre;
        int rating;
        int year = 0;
        String selection;
        String ratingSout = "";
        String genreSout = "";
        String yearSout = "";
        String ratingS = "";
        String genreS = "";
        String yearS = "";
        try {
            selection = request.getParameter("sel");
            switch (selection) {
                case "List":
                    //list:  List all Movies in the collection
                    out.println("<p>List all Movies in the collection</p>");
                    TestDAO t = TestDAO.getInstance();
                    List<Movie> m = t.getMovies();
                    for (Movie i : m) {
                        //List Database table movie
                        out.println("<p>" + i + "</p>");
                        //System.out.println(i);
                    }
                    break;
                case "Search":
                    //search:  Search for a specific movie
                    out.println("<p>Search for a specific movie</p>");
                    try {
                        titleSearch = "";
                        titleSearch = request.getParameter("searchName");
                        TestDAO ts = TestDAO.getInstance();
                        ts.getMovies();

                    } catch (Exception e) {
                        out.println("<p>Invalid Title. Please Try again.</p>");
                    }
                    break;
                case "Add":
                    //add:  Add a movie to the collection
                    out.println("<p>Add a movie to the collection</p>");
                    try {
                        titleAdd = "";
                        titleAdd = request.getParameter("titleAddIn");
                    } catch (Exception e) {
                        out.println("<p>Invalid Title. Please Try again.</p>");
                    }
                    try {
                        genreS = "";
                        genre = 0;
                        Integer.parseInt(genreS);
                        genreS = request.getParameter("genreIn");
                        switch (genre) {
                            case 1: {
                                genreSout = "Family";
                                validAdd = true;
                                break;
                            }
                            case 2: {
                                genreSout = "Drama";
                                validAdd = true;
                                break;
                            }
                            case 3: {
                                genreSout = "Action";
                                validAdd = true;
                                break;
                            }
                            case 4: {
                                genreSout = "Adventure";
                                validAdd = true;
                                break;
                            }
                            case 5: {
                                genreSout = "Comedy";
                                validAdd = true;
                                break;
                            }
                            case 6: {
                                genreSout = "Documentary";
                                validAdd = true;
                                break;
                            }
                            case 7: {
                                genreSout = "Horror";
                                validAdd = true;
                                break;
                            }
                            case 8: {
                                genreSout = "Suspense";
                                validAdd = true;
                                break;
                            }
                            case 9: {
                                genreSout = "Thriller";
                                validAdd = true;
                                break;
                            }
                            default:
                                validAdd = false;
                                out.println("<p>Invalid Genre. Please Try again.</p>");
                                break;
                        }
                    } catch (Exception e) {
                        out.println("<p>Invalid Genre. Please Try again.</p>");
                    }
                    try {
                        ratingS = "";
                        rating = 0;
                        ratingS = request.getParameter("ratingIn");
                        Integer.parseInt(ratingS);
                        switch (rating) {
                            case 1: {
                                ratingSout = "G";
                                validAdd = true;
                                break;
                            }
                            case 2: {
                                ratingSout = "PG";
                                validAdd = true;
                                break;
                            }
                            case 3: {
                                ratingSout = "PG-13";
                                validAdd = true;
                                break;
                            }
                            case 4: {
                                ratingSout = "R";
                                validAdd = true;
                                break;
                            }
                            case 5: {
                                ratingSout = "NR";
                                validAdd = true;
                                break;
                            }
                            default: {
                                out.println("<p>Invalid Rating. Please Try again.</p>");
                                validAdd = false;
                                break;
                            }
                        }
                    } catch (Exception e) {
                        out.println("<p>Invalid Rating. Please Try again.</p>");
                    }
                    try {
                        yearS = "";
                        year = 0;
                        yearS = request.getParameter("yearIn");
                        year = Integer.parseInt(yearS);
                        if ((year > 1800) && (year < 2200)) {
                            validAdd = true;
                        } else {
                            validAdd = false;
                            out.println("<p>Invalid Year. Please Try again.</p>");
                        }
                    } catch (Exception e) {
                        out.println("<p>Invalid Year. Please Try again.</p>");
                        if (validAdd == true) {
                            //Add table to database
                            TestDAO ta = TestDAO.getInstance();
                            ta.addMovie(titleAdd, genreSout, ratingSout, year);

                        } else {
                            out.println("<p>Invalid Input. Please Try again.</p>");
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            out.println("<p>Error - Invalid selection</p>");
        }
    }
}
