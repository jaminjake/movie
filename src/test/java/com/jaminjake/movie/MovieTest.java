package com.jaminjake.movie;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovieTest {
private Movie mtest = new Movie();
    @Test
    void getMovieId() {
        assertEquals(1,1);
    }

    @Test
    void getMovieName() {
        assertEquals("Bourne Identity","Bourne Identity");
    }

    @Test
    void getMovieGenre() {
        assertEquals("Family","Family");
    }

    @Test
    void getMovieRating() {
        assertEquals("PG-13","PG-13");
    }
}